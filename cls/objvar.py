#!/usr/bin/python
#filename objvar.py


class Person:
	'''Class Person Start'''
	population=0;
	
	def __init__(self,name):
		'''Initializes the person's data'''
		self.name=name
		print 'Init %s ' % self.name
		Person.population += 1
	
	def __del__(self):
		'''I am dying'''
		print '%s is says bye' % self.name
		
		Person.population -= 1
	
		if(Person.population == 0):
			print 	'population is last one';
		else:
			print   'There are %d people left' % Person.population

	def sayHi(self):
		'''Greeting by the person
	
		Really, that's all it does.'''
		
		print 'Hi,my name is %s.' % self.name

	def howMany(self):
		'''How many'''
		if Person.population == 0:
			print 'population is 0';
		else:
			print 'We have %d persons here' % Person.population;



p = Person('zzw')
p.sayHi()
p.howMany()

k=Person('kk')
k.sayHi()
k.howMany()
