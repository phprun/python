#!/usr/bin/python

class SchoolMember:
	'''Represents any school'''
	def __init__(self,name,age):
		self.name = name
		self.age = age
		print 'Init School %s ' % self.name
	
	def tell(self):
		'''Tell my details'''
		print 'my name is %s, my age is %d' % (self.name, self.age)
	
	

class Teacher(SchoolMember):
	'''Represents a teacher'''
	def __init__(self, name, age, salary):
		SchoolMember.__init__(self,name,age)
		self.salary = salary
		print 'Init Teacher : %s' % self.name
	
	def tell(self):
		SchoolMember.tell(self)
		print 'Salary : "%d"' % self.salary


class Student(SchoolMember):
	'''Represents a Student'''
	def __init__(self,name,age,marks):
		SchoolMember.__init__(self,name,age)
		self.marks=marks
		print '(Initialized Student: %s)' % self.name

	def tell(self):
        	SchoolMember.tell(self)
        	print 'Marks: "%d"' % self.marks	


t = Teacher('zzw',28,30000);
t.tell();
s = Student('student',30,9999);
s.tell()
